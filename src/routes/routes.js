const { Router } = require("express");
const StoreController=require("../controllers/StoreController");
const ProductController = require("../controllers/ProductController");
const UserController = require("../controllers/UserController");
const router = Router();

//Users routes

router.post("/Users", UserController.create);
router.get("/Users /:id", UserController.show); 
router.get("/Users", UserController.index); 
router.put("/Users/:id", UserController.update);
router.delete("/Users/:id", UserController.destroy);

//products routes

router.post("/products", ProductController.create);
router.get("/products/:id", ProductController.show); 
router.get("/products", ProductController.index); 
router.put("/products/:id", ProductController.update);
router.delete("/products/:id", ProductController.destroy);

//stores routes

router.post("/stores", StoreController.create);
router.get("/stores /:id", StoreController.show); 
router.get("/stores", StoreController.index); 
router.put("/stores/:id", StoreController.update);
router.delete("/stores/:id", StoreController.destroy);

module.exports = router;