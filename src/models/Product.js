const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define('Product', {
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price:{
        type: DataTypes.DOUBLE,
        allowNull:false
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    photo:{
        type: DataTypes.TEXT,
        allowNull: false
    },
    payment_method: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Product.associate = function(models) {
    Product.belongsTo(models.User);
    Product.belongsTo(models.Store);
};


module.exports = Product;