const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define("User",{
    rg: {
        type: DataTypes.STRING,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING,
        allowNull: false
    },
    birth_date:{
        type: DataTypes.STRING,
        allowNull: false
    },
    phone_number:{
        type: DataTypes.STRING,
        allowNull: false
    },
    password:{
        type: DataTypes.STRING,
        allowNull: false
    }

});

User.associate = function(models) {
    User.hasMany(models.Product);
};

module.exports = User;